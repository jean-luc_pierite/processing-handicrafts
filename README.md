# Processing Handicrafts
- [Demo Page](https://jean-luc_pierite.gitlab.io/processing-handicrafts/)
- [STEAM Workshop](assets/STEAM Workshop.pdf)
    - Rita Veronica Agreda de Pazos and Jean-Luc Pierite
    - MDEF 2020/21
    - Institut d'Arquitectura Avançada de Catalunya
    - 19/02/2021

## Table of Contents
1. [Project Ideation](#project-ideation)
2. [Project Development](#project-development)
3. [Concepts](#concepts)
4. [Skills](#skills)

## Project Ideation

Inspired in Carnival, Culture, Art, STEAM we decided to create an intervention artifact. To create it we will combine laser cutting, computer aided design, and simple circuits.

Our purpouse is to engage women and girls in artisan making communities.

This intervention will respond to the cultural crisis cultivated by the pandemic while also conceiving of future carnaval festivities.

The artifact will be a speculative exploration of a Carnival on a spaceship in a billion seconds: the year 2053.

**Reflexions and questions:**

1. How are land-based festivities represented?
    - [Carnivals around the World](https://en.unesco.org/news/carnivals-around-world)
2. What new expressions of Carnival will exist in the future?
3. How can we use generative art to aid artisans in prototyping?

The artifact could be part of a system of design tools for artisans making future projects.

Many of the artisans are using months of their time and thousands of dollars in resources to build elaborated costumes.

What will be the relevance of this practice and how will it be preserved in the future with expanded engagement with digital technologies?

**References:**

- Culturally Situated Design Tools:
    - [Link](https://csdt.org/)

- Mardi Gras Patch with Algorithmic Design:

    - [Link](https://www.scopesdf.org/scopesdf_lesson/making-a-mardi-gras-inspired-patch-using-algorithmic-design/)

- Processing Generative Webcam:
    - [Link](https://www.behance.net/gallery/12118151/Processing-Generative-WebCam)

**Fab Academy Content:**
- Lasercutter
- Vinyl cutter
- Circuit design
- HTML
- Computer Aided Design - Processing

**Community / Area of Practice**
- STEAM Educators
- Women artisans
- Cultural preservation activists

**Area of Interest / Research Topic**
- Digital fabrication and handicrafts
- Computer aided design and artisan making
- Cultural heritage
- Land-based knowledge

**Atlas of Weak Signals**
- Imagining non-Western centric futures

*Back to [top](#processing-handicrafts)*

## Project Development

![](images/160129115710_carnaval_de_oruro_976x549_fellipeabreu_nocredit.jpg)

Speculating on carnaval practices in the future, we acknowledge that practices are land-based and vary by geography. How would future people experience carnaval on a spaceship? How do cultural narratives merge? How can we simulate the experience to explore digital design tools for artisan making?

In a STEAM learning experience targeted at women and girls, we will use the form of a moebius strip as a metaphor for: the butterfly effect, interdependence, and preservation of local knowledge. How do our actions impact the local ecosystem and future generations? How do cultural narratives shift through digital design?

![](images/IMG_1012_2000x.jpg)

The main lasercut form will be the moebius strip. The design is based on one from Jeremy Baumberg:
- [Link](https://obrary.com/products/moebius-strip)

![](images/captura.jpg)

We will integrate paper circuits to use LED lights as the paper torches from Volt, Paper, Scissors:
- [Link](https://www.voltpaperscissors.com/paper-torch)

![](images/silk.jpg)

We will use Processing to input webcam data to do generative art as documented here:
- [Link](https://www.youtube.com/watch?v=9Z6unDwuq6w)

**Prototyping**

![](images/IMG_2300.jpg)

First prototype with paper model

**Working with Rhino**
- [Link](images/rhino.mp4)

**Working with Inkscape**
- [Link](images/inkscape\ moebius.mp4)

**Focusing the Trotec Speedy 400**

[Link](images/IMG_2301.mp4)

**Cutting with Trotec Speedy 400**

[Link](images/IMG_2302.mp4)

**Drawing rectangles with p5**

[Link](images/p5 drawing rects.mp4)

**Camera input with p5**

[Link](images/p5 camera input.mp4)

**First cuts**

![](images/IMG_2303.jpg)

First lasercut piece. Material is 4mm cardboard. Power on a Trotec Speedy 400 is 40; Speed is 1; Frequency is 1000 Hz

*Back to [top](#processing-handicrafts)*

## Concepts

1. What is a Fab Lab?
    - [Link](https://fabfoundation.org/getting-started/#fablabs-full)
2. Guiding principles and philosophy of Fab Labs such as open source
    - [The Fab Charter](https://fab.cba.mit.edu/about/charter/)
3. What is version control and why it is important
    - [Link](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week01/gitbasics/)
4. How does Internet work ?
    - [How the Internet Travels Across Oceans](https://www.nytimes.com/interactive/2019/03/10/technology/internet-cables-oceans.html)
5. Git principles (push, pull, stage)
    - [Link](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/material/extras/week01/gitbasics/#basic-changes-workflow)
6. Understand what is a File System
    - [Unix / Linux - File System Basics](https://www.tutorialspoint.com/unix/unix-file-system.htm)
7. Web development
    - [HTML Responsive Web Design](https://www.w3schools.com/html/html_responsive.asp)
8. Vector and raster graphics properties
    - [Scalable Vector Graphics (SVG) 2](https://www.w3.org/TR/SVG2/)
    - [Overview of JPEG](https://jpeg.org/jpeg/)
    - [Portable Network Graphics (PNG) Specification (Second Edition)](https://www.w3.org/TR/2003/REC-PNG-20031110/)
9. Parametric design
    - [FreeCAD - Parametric Objects](https://wiki.freecadweb.org/Manual:Parametric_objects)
10. Designing for manufacture (e.g. understanding of tolerances & lasercutting kerf)
    - [Kerf Spacing Calculator for Bending Wood](https://www.blocklayer.com/kerf-spacing.aspx)
11. Managing machinery in the lab safely
    - [Fab Lab BCN - Safety Protocols](https://wiki.fablabbcn.org/Category:Safety_Protocol)
12. Understanding materials properties
    - [Laser Cutter Materials](http://wiki.atxhs.org/wiki/Laser_Cutter_Materials)
    - [Curved Laser Bent Wood](https://www.instructables.com/Curved-laser-bent-wood/)
    - [Five Types of Recognized Forces](https://www.slideshare.net/notesmaster/2a-structures-compression-torsion-shear-bending-tension-stress-amp-strain-fo-s-good-ppt)
13. Parallel vs Series circuits
14. Understanding role of basic electronics components (resistor, capacitor, diodes)
15. Process and importance of debugging
16. Understanding what a bootloader is.
17. Understanding what an ISP is.

*Back to [top](#processing-handicrafts)*

## Skills

1. Planning - Ideation
    - [Miro Board](https://miro.com/app/board/o9J_lUHzxsI=/)
2. GitLab navigation
    - [FabAcademy BCN Local Documentation](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/videoclasses21/#how-to-create-a-repository-micro-challenge)
3. IDE navigation
    - [Atom](https://flight-manual.atom.io/getting-started/sections/atom-basics/)
    - [Eclipse](https://www.eclipse.org/getting_started/)
    - [Processing](https://processing.org/reference/environment/)
4. File optimization for web (e.g. image compression)
    - [Image Magick](https://imagemagick.org/script/download.php)
    - [Fab Academy - image encoding](http://academy.cba.mit.edu/classes/computer_design/image.html)
    - [Fab Academy - video encoding](http://academy.cba.mit.edu/classes/computer_design/video.html)
5. Basic understanding of HTML, CSS, Markdown
    - [W3 HTML Tutorial](https://www.w3schools.com/html/)
    - [W3 CSS Tutorial](https://www.w3schools.com/w3css/defaulT.asp)
    - [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
6. Command of 2D software & designing in 2D
    - [Inkscape]()
7. Command of 3D software & designing in 3D
    - [Rhino]()
8. Understand 2D & 3D file types & properties (e.g. nurbs vs mesh modelling)
9. Rendering / Animation
10. Preparing CAD files for the laser cutter
11. Using the laser cutter & adjusting settings for specific materials
12. Preparing CAD files for the vinyl cutter
13. Using the vinyl cutter and & adjusting settings for specific materials
14. Using traces and outlines in Fab Modules or Mods App
15. Command of milling & software machine and milling a board
16. Changing an endmill in the milling machine correctly
17. Soldering & Debugging  milled board
18. Uploading a makefile onto a milled and soldered board

*Back to [top](#processing-handicrafts)*
