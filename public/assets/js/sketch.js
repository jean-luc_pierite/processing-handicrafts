/**
 * Processing Handicrafts
 * Video Capture example: https://p5js.org/examples/dom-video-capture.html
 * Video pixels example: https://p5js.org/examples/dom-video-pixels.html 
 * p5 SVG: https://github.com/zenozeng/p5.js-svg
*/

// video capture
let capture;

// delay timer
let startTime;
let delayTime = 3000;

let shapes = 0;

// This is called first
function setup()
{
  startTime = millis();
  createCanvas(1280, 720);
  
  // video feed
  capture = createCapture(VIDEO);
  capture.size(1280, 720);
  // uncomment to hide video
  capture.hide();
  noCursor();
}

function mousePressed()
{
	save();
}

// This updates the window
function draw()
{

  capture.loadPixels();
  
  const stepSize = round(constrain(mouseX / 8, 6, 32));
  for (let y = 0; y < height; y += stepSize) {
    for (let x = 0; x < width; x += stepSize) {
      const i = y * width + x;
      const darkness = (255 - capture.pixels[i * 4]) / 255;
      const radius = stepSize * darkness;
      fill(mouseX, mouseY, (mouseX - i));
    if (shapes % 3 == 0)
      {
          ellipse(x, y, radius, radius);
    	  
      } else {
        if (shapes % 2 == 0)
          {
          triangle((x - radius /2), (y + radius /2), x, (y - radius /2), (x + radius /2), y);            
          } else {
          triangle(x, (y + radius /2), (x - radius /2), y, (x + radius /2), (y - radius /2));            
          }
    	  
      }
      shapes++;
    }
  }
  
}
